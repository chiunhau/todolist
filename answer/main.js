var app = new Vue({
  el: '#app',
  data: {
    newTodoText: '',
    checkedTodos: [],
    withDone: true,
    todos: []
  },
  methods: {
    addTodo: function() {
      var self = this;
      $.post({
        url: 'http://chiunhau9.todolist.simpleinfo.tw/task',
        data: {
          content: this.newTodoText.toString()
        },
        dataType: 'json',
        success: function(data) {
          self.todos.push(data);
          self.newTodoText = '';
        }
      });
    },
    del: function(todo, i) {
      $.post({
        url: 'http://chiunhau9.todolist.simpleinfo.tw/task/' + todo.id + '/delete',
        success: function(data) {
          console.log(data);
        }
      });

      this.todos.splice(i, 1);
      
    }
  },
  mounted: function(){
    console.log("going to dig u~~");
    var self = this;
    $.get({
      url: 'http://chiunhau9.todolist.simpleinfo.tw/task',
      dataType: 'json',
      success: function(data) {
        self.todos = data;
      
      }
    })
  }
})